## MongoDB setup

Create a MongoDB database named `tutorial_db` (See details at file `app/config/db.config.js`)

## Project setup
```
npm install
```

### Run
```
npm start
```
